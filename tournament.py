#!/usr/bin/env python
# 
# tournament.py -- implementation of a Swiss-system tournament
#
# Author: Ehsan Moravveji
# Date: 02 January 2017

import sys
import psycopg2


def connect():
    """Connect to the PostgreSQL database.  Returns a database connection."""
    return psycopg2.connect("dbname=tournament")

def cursor():
    """Get the cursor"""
    conn = connect()
    return conn.cursor()

def commit_close(connection):
    """Commit the changes and close the connection to a DB"""
    connection.commit()
    connection.close()

def deleteMatches():
    """Remove all the match records from the database."""
    conn = connect()
    curs = conn.cursor()
    cmnd = 'delete from match'
    curs.execute(cmnd)
    # cmnd 'update player set n_match = 0, n_win = 0, n_loss = 0'
    curs.execute('update player set n_match = 0')
    curs.execute('update player set n_win = 0')
    curs.execute('update player set n_loss = 0')
    conn.commit()
    # curs.close()
    conn.close()


def deletePlayers():
    """Remove all the player records from the database."""
    conn = connect()
    curs = conn.cursor()
    cmnd = 'delete from player'
    curs.execute(cmnd)
    conn.commit()
    conn.close()


def countPlayers():
    """Returns the number of players currently registered."""
    conn = connect()
    curs = conn.cursor()
    cmnd = 'select count(id) as cnt from player'
    curs.execute(cmnd)
    conn.commit()

    count = curs.fetchone()[0]
    conn.close()

    return count


def registerPlayer(name):
    """Adds a player to the tournament database.
  
    The database assigns a unique serial id number for the player.  (This
    should be handled by your SQL database schema, not in your Python code.)
  
    Args:
      name: the player's full name (need not be unique).
    """
    conn = connect()
    curs = conn.cursor()
    curs.execute("insert into player (name) values (%s)", (name, ))
    conn.commit()
    conn.close()

def playerStandings():
    """Returns a list of the players and their win records, sorted by wins.

    The first entry in the list should be the player in first place, or a player
    tied for first place if there is currently a tie.

    Returns:
      A list of tuples, each of which contains (id, name, wins, matches):
        id: the player's unique id (assigned by the database)
        name: the player's full name (as registered)
        wins: the number of matches the player has won
        matches: the number of matches the player has played
    """
    conn = connect()
    curs = conn.cursor()
    cmnd = 'select id, name, n_win, n_match from player order by n_win desc'
    curs.execute(cmnd)
    conn.commit()
    res  = curs.fetchall()

    return res


def reportMatch(winner, loser):
    """Records the outcome of a single match between two players.

    Args:
      winner:  the id number of the player who won
      loser:  the id number of the player who lost
    """
    conn = connect()
    curs = conn.cursor()
    # get number of wins
    # cmnd_match  = 'insert into match (id_player_1, id_player_2) values ({0}, {1})'.format(winner, loser)
    curs.execute("insert into match (id_winner, id_loser) values (%s, %s)", (str(winner), str(loser)))

    cmnd_winner = 'update player set n_match = n_match + 1, n_win = n_win + 1 where id = {0}'.format(winner)
    cmnd_loser  = 'update player set n_match = n_match + 1, n_loss = n_loss + 1 where id = {0}'.format(loser)
    curs.execute(cmnd_winner)
    curs.execute(cmnd_loser)
    conn.commit()
    conn.close()
 
 
def swissPairings():
    """Returns a list of pairs of players for the next round of a match.
  
    Assuming that there are an even number of players registered, each player
    appears exactly once in the pairings.  Each player is paired with another
    player with an equal or nearly-equal win record, that is, a player adjacent
    to him or her in the standings.
  
    Returns:
      A list of tuples, each of which contains (id1, name1, id2, name2)
        id1: the first player's unique id
        name1: the first player's name
        id2: the second player's unique id
        name2: the second player's name
    """
    standings = playerStandings()
    n_players = len(standings)
    n_pairs   = n_players / 2

    X         = standings[:]
    result    = []
    for i_pair in range(n_pairs):
        k1    = 2 * i_pair
        k2    = k1 + 1
        result.append( (X[k1][0], X[k1][1], X[k2][0], X[k2][1]) )

    return result


