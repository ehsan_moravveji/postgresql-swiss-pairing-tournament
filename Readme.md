
# A Small Project in Relational Databases

## What is in there?
The current set of files correspond to an online course on the [Udacity](https://www.udacity.com)
with the title **Intro to Relational Databases**. The course is introductory, but very comprehensive
and pragmatic. The course had a final quiz, which the explanation is also put in this archive as 
the [Quiz Description](Project Description - tournament.pdf "Click to see the quiz text in PDF format").

## The Swiss Pairing Tournament
The game tournament will use the Swiss system for pairing up players in each round: 
players are not eliminated, and each player should be paired with another player with the same number of wins, 
or as close as possible. This project has two parts: defining the database schema (SQL table definitions), 
and writing the code that will use it.

## File Descriptions

 * [Database Schema "tournament.sql"](tournament.sql) is the SQL file that creates the relevant tables,
 * [Tournament Module "tournament.py"](tournament.py) is a Python file with several functions to carry out the 
   Swiss Tournament database handling, and player rankings,
 * [Unit Tests "tournament_test.py"](tournament_test.py) is a Python script that carries out 10 tests to ensure all 
   required functionality for the Swiss Tournament is properly implemented. This is the file one
   can execute.