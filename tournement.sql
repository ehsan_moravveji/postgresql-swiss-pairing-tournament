
-- The schema for the final quiz of the Udacity (https://www.udacity.com) course:
-- "Intro to Relational Databases"

-- Author: Ehsan Moravveji
-- Date: 02 January 2017

create table tournament (
  id        integer primary key,
  n_matches integer not null
);

create table player (
  id        serial,
  id_trnmt  integer references tournament (id),
  name      text not null,
  n_match   integer default 0,
  n_win     integer default 0,
  n_loss    integer default 0,

  primary key (id)

);

create table match (
  id          serial primary key,
  id_trnmt    integer references tournament (id),
  id_winner   integer references player (id),
  id_loser    integer references player (id),

  unique (id_winner, id_loser),
  constraint reject_self check (id_winner != id_loser)

);

